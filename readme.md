# SuitedConnector PaladinHelper

This repository serves as a Composer package containing a helper library for interacting with the Paladin (internal opt-out lists for email and phone) system.

## Including in an existing project

Add the following in your `composer.json` file

```
    "repositories": [
        {
            "type":"vcs",
            "url": "https://bitbucket.org/suitedconnector/paladin-helper.git"
        }
    ],
    "require": {
        "suitedconnector/paladin-helper": "dev-master"
    },
```

The helper library is added to the `App\Library\Utility` namespace, and will be usable with:

```
use App\Library\Utility\PaladinHelper;
```
