<?php

namespace App\Library\Utility;

use App\Library\Traits\APIConsumerTrait;
use App\Library\Traits\ErrorTrait;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Log;


class PaladinHelper {
	use ErrorTrait;
	use APIConsumerTrait;

	const BASE_URI = 'https://paladin.suitedconnector.com';
	const EMAIL_PATH = '/email/';
	const PHONE_PATH = '/phone/';
	const COMBO_PATH = '/optin';
	const DETAILS_LIMIT = 256;
	const EMAIL_LIMIT = 254;

	const CONNECTION_TIMEOUT = 45;

	protected $httpClient;

	/**
	 * PaladinHelper constructor.
	 */
	public function __construct() {
		$this->reset();
		$this->httpClient = new HttpClient([
		 'base_uri' => self::BASE_URI,
		 'synchronous' => TRUE,
		 'verify' => FALSE,
		 'connect_timeout' => self::CONNECTION_TIMEOUT,
		 'version' => 1.0
		]);
	}

	/**
	 * @param string $email
	 *
	 * @return bool
	 */
	public function checkEmail(string $email) {
		$this->reset();
		if ($this->validEmail($email)) {
			try {
				$response = $this->httpClient->request('GET', self::EMAIL_PATH.$email);
				$this->consumeAPIDetails($this->evaluateAPIResponse($response->getStatusCode(), (string)$response->getBody(), 'blocked'));
			}
			catch (\Exception $e) {
				$this->setError("Paladin communication exception for {$email}: " . $e->getMessage());
			}
		} else {
			$this->setError("Invalid email: {$email}");
		}
		if ($this->isError()) {
			$this->log();
		}
		return $this->results();
	}

	/**
	 * @param string $phone
	 *
	 * @return bool
	 */
	public function checkNumber(string $phone) {
		$this->reset();
		if ($this->validPhone($phone)) {
			try {
				$response = $this->httpClient->request('GET', self::PHONE_PATH . $phone);
				$this->consumeAPIDetails($this->evaluateAPIResponse($response->getStatusCode(), (string)$response->getBody(), 'blocked'));
			}
			catch (\Exception $e) {
				$this->setError("Paladin communication exception for {$phone}: " . $e->getMessage());
			}
		} else {
			$this->setError("Invalid phone: {$phone}");
		}
		if ($this->isError()) {
			$this->log();
		}
		return $this->results();
	}

	/**
	 * @param string $client - helps with logging, tells where the call came from
	 * @param string $phone
	 *
	 * @return bool
	 */
	public function checkNumberDbOnly(string $client, string $phone) {
		$this->reset();
		Log::info("PaladinHelper.checkNumberDbOnly() -> for client: {$client} phone: {$phone}");
		if ($this->validPhone($phone)) {
			$response = $this->httpClient->request('GET', self::PHONE_PATH . $client . '/' . $phone);
				$this->consumeAPIDetails($this->evaluateAPIResponse($response->getStatusCode(), (string)$response->getBody(), 'blocked'));
		} else {
			$this->setError("Invalid phone: {$phone}");
		}
		if ($this->isError()) {
			$this->log();
		}
		return $this->results();
	}

	/**
	 * @param string $email
	 *
	 * @return bool
	 */
	public function removeEmail(string $email) {
		$this->reset();
		if ($this->validEmail($email)) {
			try {
				$response = $this->httpClient->request('DELETE', self::EMAIL_PATH . $email);
				$this->consumeAPIDetails($this->evaluateAPIResponse($response->getStatusCode(), (string)$response->getBody()));
			}
			catch (\Exception $e) {
				$this->setError("Paladin communication exception for {$email}: " . $e->getMessage());
			}
		} else {
			$this->setError("Invalid email: {$email}");
		}
		if ($this->isError()) {
			$this->log();
		}
		return $this->results();
	}

	/**
	 * @param string $phone
	 *
	 * @return bool
	 */
	public function removeNumber(string $phone) {
		$this->reset();
		if ($this->validPhone($phone)) {
			try {
				$response = $this->httpClient->request('DELETE', self::PHONE_PATH . $phone);
				$this->consumeAPIDetails($this->evaluateAPIResponse($response->getStatusCode(), (string)$response->getBody()));
			}
			catch (\Exception $e) {
				$this->setError("Paladin communication exception for {$phone}: " . $e->getMessage());
			}
		} else {
			$this->setError("Invalid phone: {$phone}");
		}
		if ($this->isError()) {
			$this->log();
		}
		return $this->results();
	}

	/**
	 * @param string $email
	 * @param string $details
	 *
	 * @return bool
	 */
	public function addEmail(string $email, string $details='') {
		$this->reset();
		if ($this->validEmail($email)) {
			try {
				$response = $this->httpClient->request('POST', self::EMAIL_PATH . $email, [
					'query' => [
						'details' => $details
					]
				]);
				$this->consumeAPIDetails($this->evaluateAPIResponse($response->getStatusCode(), (string)$response->getBody()));
			}
			catch (\Exception $e) {
				$this->setError("Paladin communication exception for {$email}: " . $e->getMessage());
			}
		} else {
			$this->setError("Invalid email: {$email}");
		}
		if ($this->isError()) {
			$this->log();
		}
		return $this->results();
	}

	/**
	 * @param string $phone
	 * @param string $details
	 *
	 * @return bool
	 */
	public function addNumber(string $phone, string $details='') {
		$this->reset();
		if ($this->validPhone($phone)) {
			try {
				$response = $this->httpClient->request('POST', self::PHONE_PATH . $phone, [
					'query' => [
						'details' => $details
					]
				]);
				$this->consumeAPIDetails($this->evaluateAPIResponse($response->getStatusCode(), (string)$response->getBody()));
			}
			catch (\Exception $e) {
				$this->setError("Paladin communication exception for {$phone}: " . $e->getMessage());
			}
		} else {
			$this->setError("Invalid phone: {$phone}");
		}
		if ($this->isError()) {
			$this->log();
		}
		return $this->results();
	}

	/**
	 * @param string $email
	 * @param string $phone
	 *
	 * @return bool
	 */
	public function optinBoth(string $email, string $phone) {
		$this->reset();
		if ($this->validEmail($email) && $this->validPhone($phone)) {
			try {
				$response = $this->httpClient->request('GET', self::COMBO_PATH, [
					'query' => [
						'email' => $email,
						'phone' => $phone
					]
				]);
				$this->consumeAPIDetails($this->evaluateAPIResponse($response->getStatusCode(), (string)$response->getBody()));
			}
			catch (\Exception $e) {
				$this->setError("Paladin communication exception for {$email}, {$phone}: " . $e->getMessage());
			}
		} else {
			$this->setError("Invalid email {$email} or phone {$phone}");
		}
		if ($this->isError()) {
			$this->log();
		}
		return $this->results();
	}

	/**
	 * Helper function to decode the combined message for combine optin single call that covers both email and phone
	 *
	 * @return array
	 */
	public function decodeOptinResults() {
		$decodedResults = [
			'email' => FALSE,		// FALSE = okay to use, TRUE = on blacklist
			'phone' => FALSE		// FALSE = okay to use, TRUE = on blacklist
		];
		if ($this->isFailure()) {
			// one case where we need to inspect the message to determine which of the two is on the permanent blacklist
			$message = $this->getMessage();
			if (strlen($message)>0) {
				/*
				 * Messages look like this coming back from Paladin for this combined call:
				 * Email {$email} is on blacklist.  Phone {$phone} is on blacklist.
				 */
				if (strpos($message,'Email') !== FALSE) {
					$decodedResults['email'] = TRUE;
				}
				if (strpos($message, 'Phone') !== FALSE) {
					$decodedResults['phone'] = TRUE;
				}
			}
		}
		return $decodedResults;
	}

	/**
	 * @param string $phone
	 *
	 * @return bool
	 */
	static public function validPhone(string &$phone = null) {
		if (isset($phone)) {
			// enforce all digits and limit
			$phone = preg_replace('/(\D+)/', '', $phone);
			// take only the last 10 digits
			if (strlen($phone) > 10) {
				$phone = substr($phone, strlen($phone) - 10, 10);
			}
		}
		return (isset($phone) && (strlen($phone) === 10));
	}

	/**
	 * @param string $email
	 *
	 * @return bool
	 */
	static public function validEmail(string &$email = null) {
		if (isset($email)) {
			// enforce limit
			$email = str_limit($email, 254);
		}
		return (isset($email) && filter_var($email, FILTER_VALIDATE_EMAIL));
	}
}
